package com.devcamp.province_crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvinceCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvinceCrudApplication.class, args);
	}

}
