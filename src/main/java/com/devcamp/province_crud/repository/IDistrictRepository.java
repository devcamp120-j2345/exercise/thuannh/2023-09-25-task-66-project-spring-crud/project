package com.devcamp.province_crud.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province_crud.model.District;
import com.devcamp.province_crud.model.Province;

public interface IDistrictRepository extends JpaRepository<District, Long>{
    District findById(int id);
    List<District> findByProvinceId(int id);
    Optional<Province> findByIdAndProvinceId(Long id, Long instructorId);
}
