package com.devcamp.province_crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province_crud.model.Province;

public interface IProvinceRepository extends JpaRepository<Province, Long>{
    Province findById(int id);
    
}
