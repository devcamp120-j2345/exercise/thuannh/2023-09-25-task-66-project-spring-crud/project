package com.devcamp.province_crud.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province_crud.model.District;
import com.devcamp.province_crud.model.Ward;

public interface IWardRepository extends JpaRepository<Ward, Long> {
    Ward findById(int id);
    List<Ward> findByDistrictId(int id);
    Optional<District> findByIdAndDistrictId(Long id, Long instructorId);
}
