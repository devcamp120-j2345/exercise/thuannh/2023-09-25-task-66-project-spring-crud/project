package com.devcamp.province_crud.services;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.province_crud.model.District;
import com.devcamp.province_crud.model.Ward;
import com.devcamp.province_crud.repository.IDistrictRepository;

@Service
public class DistrictService {
    @Autowired
    IDistrictRepository districtRepository;
    public ArrayList<District> getAllDistricts(){
        ArrayList<District> districtList = new ArrayList<>();
        districtRepository.findAll().forEach(districtList:: add);
        return districtList;
    }
    public Set<Ward> getWardsByDistrictId(int id){
        District vDistrict = districtRepository.findById(id);
        if ( vDistrict != null){
            return vDistrict.getWards();
        }
        else return null;
    }

}
